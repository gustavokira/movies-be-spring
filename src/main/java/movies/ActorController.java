package movies;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActorController{
  private final ActorRepository repository;

  ActorController(ActorRepository repository) {
    this.repository = repository;
  }

  @GetMapping("/actors")
  List<Actor> all() {
    return repository.findAll();
  }

  @PostMapping("/actors")
  Actor addActor(@RequestBody Actor actor) {
    return repository.save(actor);
  }

  @GetMapping("/actors/{id}")
  Actor getActorById(@PathVariable String id) {
    return repository.findById(id)
      .orElseThrow(() -> new ActorNotFoundException(id));
  }

  @PutMapping("/actors/{id}")
  Actor addOrUpdateActorById(@RequestBody Actor a, @PathVariable String id) {

    return repository.findById(id)
      .map(actor -> {
        actor.setName(a.getName());
        actor.setBirth(a.getBirth());
        actor.setCountry(a.getCountry());

        return repository.save(actor);
      })
      .orElseGet(() -> {
        a.setId(id);
        return repository.save(a);
      });
  }

  @DeleteMapping("/actors/{id}")
  void removeActorById(@PathVariable String id) {
    repository.deleteById(id);
  }
}
