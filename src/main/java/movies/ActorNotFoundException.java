package movies;

class ActorNotFoundException extends RuntimeException {

  ActorNotFoundException(String id) {
    super("Could not find actor " + id);
  }
}
