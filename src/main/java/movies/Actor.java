package movies;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;

@Data
@Entity
public class Actor{

  private @Id String id;
  private String name;
  private Date birth;
  private String country;

  public Actor() {}

  public Actor(String name, Date birth, String country) {
    this.name = name;
    this.birth = birth;
    this.country = country;
  }
}
